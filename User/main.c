#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "LED.h" 
#include "Key.h"
#include "light_buzzer.h"
#include "OLED.h"
#include "countSensor.h"
#include "encoder.h"
#include "Timer.h"
#include "pwm.h"
#include "servo.h"
#include "motor.h"
#include "input_capture.h"
#include "tim_encoder.h"
#include "adc.h"
#include "dma.h"
#include "serial.h"
#include <time.h>
#include "my_flash.h"
#include "my_i2c.h"
#include "my_mpu6050.h"
#include "w25q64.h"

void test_oled(void);
void test_encoder(void);
void test_pulse_light(void);

uint16_t Num; // 给Timer.c引用的外部变量

int16_t encoder_speed = 0;


int main(void)
{
    // 中断优先级全局只能设置一次即可
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    // OLED作为调试工具，需要初始化
    OLED_Init();



}

// 测试硬件mpu6050
void test_hardware_mpu6050(void)
{
    MPU6050_Hardware_Init();
    // 读取MPU6050的地址
    uint8_t data = MPU6050_Hardware_ReadReg(0x75);
    OLED_ShowHexNum(1, 1, data, 2);

    // 取消MPU6050睡眠模式，因为芯片默认是睡眠模式
    MPU6050_Hardware_WriteReg(0x6B, 0x00); // 取消睡眠
    MPU6050_Hardware_WriteReg(0x19, 0xAA);// 写入分频寄存器
    uint8_t div = MPU6050_Hardware_ReadReg(0x19); // 读出分频寄存器的值

    OLED_ShowHexNum(1, 5, div, 2);

    int16_t ax, ay, az, gx, gy, gz;
    while (1)
    {
        MPU6050_GetData(&ax, &ay, &az, &gx, &gy, &gz);
        OLED_ShowSignedNum(2, 1, ax, 5);
        OLED_ShowSignedNum(3, 1, ay, 5);
        OLED_ShowSignedNum(4, 1, az, 5);

        OLED_ShowSignedNum(2, 8, gx, 5);
        OLED_ShowSignedNum(3, 8, gy, 5);
        OLED_ShowSignedNum(4, 8, gz, 5);

        Delay_ms(100);

    }
}

// 测试软件版本MPU6050
void test_software_mpu6050(void)
{
    MPU6050_Init();
    // 读取MPU6050的地址
    uint8_t data = MPU6050_ReadReg(0x75);
    OLED_ShowHexNum(1, 1, data, 2);

    // 取消MPU6050睡眠模式，因为芯片默认是睡眠模式
    MPU6050_WriteReg(0x6B, 0x00); // 取消睡眠
    MPU6050_WriteReg(0x19, 0xAA);// 写入分频寄存器
    uint8_t div = MPU6050_ReadReg(0x19); // 读出分频寄存器的值

    OLED_ShowHexNum(2, 1, div, 2);

    while (1)
    {

    }
}



// 测试软件版I2C
void test_my_i2c(void)
{
    MyI2C_Init();
    MyI2C_Start();
    MyI2C_SendByte(0xD0); // 1101 000 0
    uint8_t ack = MyI2C_ReceiveAck();
    MyI2C_Stop();
    OLED_ShowNum(1, 1, ack, 3);
    while (1)
    {

    }

}

// 测试flash
void test_flash(void)
{

    Key_Init();
    Store_Init();
    uint8_t keyNum;
    while (1)
    {

        keyNum = Key_GetNum();
        if (keyNum == 1)
        {
            SaveData[1] ++;
            SaveData[2] += 2;
            SaveData[3] += 3;
            SaveData[4] += 4;
            Store_Save();
        }
        if (keyNum == 2)
        {
            Store_Clear();
        }
        OLED_ShowHexNum(1, 1, SaveData[0], 4);

        OLED_ShowHexNum(2, 1, SaveData[1], 4);
        OLED_ShowHexNum(2, 6, SaveData[2], 4);
        OLED_ShowHexNum(3, 1, SaveData[3], 4);
        OLED_ShowHexNum(3, 6, SaveData[4], 4);
    }
}

// 测试Flash基本读写
void test_read_or_write_flash(void)
{
    // MyFlashEraseAllPages();
    // return 0;

    MyFlashErasePages(0x0800FC00);
    MyFlash_ProgramWord(0x0800FC00, 0x12345678);
    MyFlash_ProgramHalfWord(0x0800FC10, 0xABCD);

    OLED_ShowHexNum(1, 1, MyFlashReadWord(0x0800FC00), 8);
    OLED_ShowHexNum(2, 1, MyFlashReadHalfWord(0x0800FC10), 4);
    OLED_ShowHexNum(3, 1, MyFlashReadByte(0x0800FC00), 2);

    while (1)
    {

    }

}

// 窗口看门狗
void test_wwdg(void)
{
    OLED_ShowString(1, 1, "WWDG TEST");

    if (RCC_GetFlagStatus(RCC_FLAG_WWDGRST) == SET)
    {

        OLED_ShowString(2, 1, "WWDG RST");
        Delay_ms(1000);
        OLED_ShowString(2, 1, "        ");
        RCC_ClearFlag();
    }
    else
    {
        OLED_ShowString(2, 1, "RST");
        Delay_ms(1000);
        OLED_ShowString(2, 1, "   ");
    }

    // 喂狗最晚时为50ms, 窗口时间为30ms
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE); // 开启窗口开门狗的时钟

    WWDG_SetPrescaler(WWDG_Prescaler_8); // 设置分频
    WWDG_SetWindowValue(0x40 | 21); // 第6位需要置1，所以或上0x40
    WWDG_Enable(0x40 | 54); // 第6位需要置1，所以或上0x40




    Key_Init();

    while (1)
    {
        Key_GetNum(); // 按住按键不放会阻塞程序

        OLED_ShowString(4, 1, "Feed");
        Delay_ms(20);
        // OLED_ShowString(4, 1, "    ");
        Delay_ms(20); // 不满足喂狗时间

        WWDG_SetCounter(0x40 | 54);
    }

}

// 独立看门狗
void test_iwdg(void)
{

    OLED_ShowString(1, 1, "IWDG TEST");

    if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST) == SET)
    {

        OLED_ShowString(2, 1, "IWDG RST");
        Delay_ms(1000);
        OLED_ShowString(2, 1, "        ");
        RCC_ClearFlag();
    }
    else
    {
        OLED_ShowString(2, 1, "RST");
        Delay_ms(1000);
        OLED_ShowString(2, 1, "   ");
    }

    // 喂狗时间为1000ms
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
    IWDG_SetPrescaler(IWDG_Prescaler_16);
    IWDG_SetReload(2499);
    IWDG_ReloadCounter(); // 初始化时先喂狗一次
    IWDG_Enable(); // 启动看门狗

    Key_Init();

    while (1)
    {
        Key_GetNum(); // 按住按键不放会阻塞程序
        IWDG_ReloadCounter();

        OLED_ShowString(4, 1, "Feed");
        Delay_ms(200);
        OLED_ShowString(4, 1, "    ");
        Delay_ms(600); // 不满足喂狗时间
    }

}

// 待机模式(最省电)
void test_standbymode(void)
{
    // 进入待机模式重新唤醒后代码从最头开始执行
    // 进入待机模式时，最好把各种外设也断电，这样才是最省电的
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

    // 开启PA0做为Waku引脚唤醒芯片工作
    PWR_WakeUpPinCmd(ENABLE);

    while (1)
    {
        OLED_ShowString(1, 1, "StandByMode...");
        Delay_ms(5000);  //
        OLED_Clear();
        PWR_EnterSTANDBYMode();
    }
}

// 停机模式(中等省电)+外部中断唤醒
void test_stop_exti_interrupt(void)
{

    // 进入停机模式重新唤醒后代码从PWR_EnterStopMode后执行
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
    CountSensor_Init();

    OLED_ShowString(1, 1, "Count:");

    while (1)
    {
        OLED_ShowNum(1, 7, Get_CountSensor(), 5);

        OLED_ShowString(2, 1, "Running");
        Delay_ms(500);
        OLED_ShowString(2, 1, "       ");
        Delay_ms(500);

        PWR_EnterSTOPMode(PWR_Regulator_ON, PWR_STOPEntry_WFI);

        // 重新运行SystemInit，选择HSE做为时钟源
        SystemInit();

    }
}

// 睡眠模式(一般省电)+串口发送+接收
void test_sleep_usart_send_receive(void )
{
    Serial_Init();

    uint8_t rxData;
    while (1)
    {
        if (Serial_RxFlag() == 1)
        {
            rxData = Serial_RxData();
            Serial_SendData(rxData);
            OLED_ShowHexNum(1, 8, rxData, 2);
        }
        OLED_ShowString(2, 1, "Running");
        Delay_ms(500);
        OLED_ShowString(2, 1, "       ");
        Delay_ms(500);

        __WFI();

        OLED_ShowHexNum(3, 8, rxData+1, 2);;
    }
}

// 显示主频频率，可注释或解除注释system_stm32f10x.c里的SYSCLK_FREQ_72MHz/SYSCLK_FREQ_56MHz等宏变量达到修改主频频率
void test_systemcoreclock(void)
{
    OLED_ShowString(1, 1, "SYSCLK:");
    OLED_ShowNum(1, 8, SystemCoreClock, 8);

    while (1)
    {
        OLED_ShowString(2, 1, "Running");
        Delay_ms(500);
        OLED_ShowString(2, 1, "       ");
        Delay_ms(500);
    }
}

// 测试USART通信
void test_usart(void)
{
    Serial_Init();
    // Serial_SendData(0x41);
    Serial_SendNumber(123456, 6);
    Serial_SendString("\r\n");
    Serial_SendNumber(-1234, 4);
    Serial_SendString("\r\n");
    Serial_SendString("HelloWorld!\r\n");
    uint8_t array[] = {0x41, 0x42, 0x43};
    Serial_SendArray(array, 3);
    Serial_SendString("\r\n");

    printf("num=%d\r\n", 666);
    uint8_t receiveData;
    while(1)
    {
        if (Serial_RxFlag() == 1)
        {
            receiveData = Serial_RxData();
            Serial_SendData(receiveData);
            OLED_ShowHexNum(1, 1, receiveData, 2);
        }
    }
}

// 测试寄存器与存储器之间DMA转运数据
void test_register2memory_dma(void)
{
    uint16_t data[4] = {0};

    ADC1_DMA_Init((uint32_t)data, 4);
    OLED_ShowString(1, 1, "AD0:");
    OLED_ShowString(2, 1, "AD1:");
    OLED_ShowString(3, 1, "AD2:");
    OLED_ShowString(4, 1, "AD3:");
    while (1)
    {

        ADC1_DMA_GetValue(4);
        OLED_ShowNum(1, 5, data[0], 4);
        OLED_ShowNum(2, 5, data[1], 4);
        OLED_ShowNum(3, 5, data[2], 4);
        OLED_ShowNum(4, 5, data[3], 4);

        Delay_ms(100);
    }
}

// 测试存储器之间相互转运
void test_memory2memory_dma(void)
{
    uint8_t srcData[] = {0x01, 0x02, 0x03, 0x04};
    uint8_t dstData[4] = {0};


    OLED_ShowString(1, 1, "SRC:");
    OLED_ShowHexNum(1, 5, (uint32_t)srcData, 8);
    OLED_ShowString(3, 1, "DST:");
    OLED_ShowHexNum(3, 5, (uint32_t)dstData, 8);

    DMA_M2M_Init((uint32_t) srcData, (uint32_t) dstData, 4);

    while (1)
    {
        srcData[0] ++;
        srcData[1] ++;
        srcData[2] ++;
        srcData[3] ++;

        OLED_ShowHexNum(2, 1, srcData[0], 2);
        OLED_ShowHexNum(2, 4, srcData[1], 2);
        OLED_ShowHexNum(2, 7, srcData[2], 2);
        OLED_ShowHexNum(2, 10, srcData[3], 2);

        DMA_M2M_Transfer(4);

        OLED_ShowHexNum(4, 1, dstData[0], 2);
        OLED_ShowHexNum(4, 4, dstData[1], 2);
        OLED_ShowHexNum(4, 7, dstData[2], 2);
        OLED_ShowHexNum(4, 10, dstData[3], 2);

        Delay_ms(1000);
    }
}

// 测试ADC单通道连续扫描
void test_adc_multi(void)
{
    ADC1_Multi_Init();

    uint16_t AD0, AD1, AD2, AD3;
    OLED_ShowString(1, 1, "AD0:");
    OLED_ShowString(2, 1, "AD1:");
    OLED_ShowString(3, 1, "AD2:");
    OLED_ShowString(4, 1, "AD3:");

    while (1)
    {
        AD0 = ADC1_Multi_GetValue(ADC_Channel_0);
        AD1 = ADC1_Multi_GetValue(ADC_Channel_1);
        AD2 = ADC1_Multi_GetValue(ADC_Channel_2);
        AD3 = ADC1_Multi_GetValue(ADC_Channel_3);

        OLED_ShowNum(1, 5, AD0, 4);
        OLED_ShowNum(2, 5, AD1, 4);
        OLED_ShowNum(3, 5, AD2, 4);
        OLED_ShowNum(4, 5, AD3, 4);

        Delay_ms(100);
    }
}

// 测试ADC单通道非连续扫描
void test_adc_single(void)
{
    ADC1_Single_Init();

    uint16_t ADValue;
    float Voltage;
    OLED_ShowString(1, 1, "ADValue:");
    OLED_ShowString(2, 1, "Voltage:0.00v");


    while (1)
    {
        ADValue = ADC1_Single_GetValue();
        Voltage = ((float)ADValue / 4095) * 3.3;
        OLED_ShowNum(1, 9, ADValue, 4);
        OLED_ShowNum(2, 9, Voltage, 1); // 只显示整数部分
        OLED_ShowNum(2, 11, (uint16_t)(Voltage * 100)%100, 2); // 显示小数部分

        Delay_ms(100);
    }
}

// 测试时钟编码器，配合TIM2_IRQHandler使用
void test_time_encoder(void)
{
    Internal_Timer_Init();

    Tim_Encoder_Init();
    OLED_ShowString(1, 1, "Speed:");
    while (1)
    {
        OLED_ShowSignedNum(1, 7, encoder_speed, 5);
    }
}

// 定时器每个1秒发生一次中断用于测量编码器速度
void TIM2_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) == SET)
    {
        encoder_speed = Encoder_GetSpeed();
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
    }

}

// 测试输入信号频率与点空比
void test_freq_duty(void)
{
    PWM_Input_Init();
    IC_Init();

    // 目前计数频率是1MHz(看PWM_Input_Init的设置)，目前使用的是测周法，只能测量低频的，即低于计数频率的才比较准
    PWM_Input_SetPrescaler(7200-1); // 频率: Freq = 72MHz / (PSC + 1) / 100(ARR值)
    PWM_Input_SetCompare(80); // 占空比: Duty = CCR /100

    OLED_ShowString(1, 1, "Freq:000000Hz");
    OLED_ShowString(2, 1, "Duty:000%");
    while (1)
    {
        OLED_ShowNum(1, 6, IC_GetFreq(), 6);
        OLED_ShowNum(2, 6, IC_GetDuty(), 3);
    }
}

// 测试直流电机
void test_motor(void)
{
    Motor_Init();
    uint8_t keyNum;
    int8_t speed = 0;
    int8_t step = 20;
    OLED_ShowString(1, 1, "speed:");
    while (1)
    {
        keyNum = Key_GetNum();
        if (keyNum == 1)
        {
            speed += step;
            if (speed >= 100)
            {
                step = -20;
            }
            if (speed <= -100)
            {
                step = 20;
            }
        }
        Motor_SetSpeed(speed);
        OLED_ShowSignedNum(1, 7, speed, 3);
    }
}

// 测试舵机
void test_servo(void)
{
    Servo_Init();
    Key_Init();

    uint8_t KeyNum;
    float Angle;
    OLED_ShowString(1, 1, "Angle:");
    while (1)
    {
        KeyNum = Key_GetNum();
        if (KeyNum == 1)
        {
            Angle += 30;
            if (Angle > 180)
            {
                Angle = 0;
            }
        }
        Servo_SetAngle(Angle);
        OLED_ShowNum(1, 7, Angle, 3);
    }
}

// 测试呼吸灯
void test_pulse_light(void)
{
    PWM_Init();

    uint16_t i;
    while (1)
    {
        for (i = 0; i<=100; i++)
        {
            PWM_SetCompare1(i);
            Delay_ms(10);
        }
        for (i = 0; i<=100; i++)
        {
            PWM_SetCompare1(100-i);
            Delay_ms(10);
        }
    }
}

void test_ETR_Timer(void)
{
    ETR_Timer_Init();

    OLED_ShowString(1, 1, "Num:");
    OLED_ShowString(2, 1, "CNT:");
    while (1)
    {
        OLED_ShowSignedNum(1, 5, Num, 5);
        OLED_ShowSignedNum(2, 5, Timer_GetCounter(), 5);
    }
}

void test_encoder(void)
{
    Encoder_Init();

    int16_t num;
    OLED_ShowString(1, 1, "Num:");
    while (1)
    {
        num += Get_Encoder_Count();
        OLED_ShowSignedNum(1, 5, num, 5);
    }
}

void test_oled(void)
{
    OLED_Init();
    OLED_ShowChar(1,1, 'A');
    OLED_ShowString(1, 3, "Hello World!");
    OLED_ShowNum(2, 1, 12345, 5);
    OLED_ShowHexNum(2, 7, 0xAA55, 4);
    OLED_ShowSignedNum(2, 12, 66, 2);
    OLED_ShowBinNum(3, 1, 0xAA55, 16);
    while (1)
    {

    }

}

uint8_t KeyNum = 0;
// 测试通过按钮点亮灯
void test_key_led(void)
{
    LED_Init();
    Key_Init();

    while(1)
    {
        KeyNum = Key_GetNum();
        if (KeyNum == 1)
        {
            LED1_Toggle();
        }
        if (KeyNum == 2)
        {
            LED2_Toggle();
        }
    }

}

