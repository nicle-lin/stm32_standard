//
// Created by Administrator on 2024/1/3/003.
//

#ifndef STM32_STANDARD_MY_FLASH_H
#define STM32_STANDARD_MY_FLASH_H

#include "stm32f10x.h"

uint32_t MyFlashReadWord(uint32_t address);
uint16_t MyFlashReadHalfWord(uint32_t address);
uint8_t MyFlashReadByte(uint32_t address);
void MyFlashErasePages(uint32_t address);
void MyFlashEraseAllPages(void);

void MyFlash_ProgramWord(uint32_t address, uint32_t data);
void MyFlash_ProgramHalfWord(uint32_t address, uint16_t data);

extern uint16_t SaveData[];
void Store_Init(void);
void Store_Save(void);
void Store_Clear(void);


#endif //STM32_STANDARD_MY_FLASH_H
