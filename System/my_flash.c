//
// Created by Administrator on 2024/1/3/003.
//

#include "my_flash.h"

uint32_t MyFlashReadWord(uint32_t address)
{
    return *((__IO uint32_t *)(address));
}

uint16_t MyFlashReadHalfWord(uint32_t address)
{
    return *((__IO uint16_t *)(address));
}


uint8_t MyFlashReadByte(uint32_t address)
{
    return *((__IO uint8_t *)(address));
}

void MyFlashErasePages(uint32_t address)
{
    FLASH_Unlock();
    FLASH_ErasePage(address);
    FLASH_Lock();
}

void MyFlashEraseAllPages(void)
{
    FLASH_Unlock();
    FLASH_EraseAllPages();
    FLASH_Lock();
}

void MyFlash_ProgramWord(uint32_t address, uint32_t data)
{
    FLASH_Unlock();
    FLASH_ProgramWord(address, data);
    FLASH_Lock();
}


void MyFlash_ProgramHalfWord(uint32_t address, uint16_t data)
{
    FLASH_Unlock();
    FLASH_ProgramHalfWord(address, data);
    FLASH_Lock();
}

#define SaveSize 512
#define SavePageAddress 0x0800FC00
#define FlagData 0xA5A5

uint16_t SaveData[SaveSize];
void Store_Init(void)
{
    if (MyFlashReadHalfWord(SavePageAddress) != FlagData)
    {
        MyFlashErasePages(SavePageAddress);
        SaveData[0] = FlagData;
        for (int i = 0; i < SaveSize; i++)
        {
            MyFlash_ProgramHalfWord(SavePageAddress + i*2, SaveData[i]);
        }
    }
    else
    {
        for (int i = 0; i < SaveSize; i++)
        {
            SaveData[i] = MyFlashReadHalfWord(SavePageAddress + i*2);
        }
    }
}

void Store_Save(void)
{
    MyFlashErasePages(SavePageAddress);
    for (int i = 0; i < SaveSize; i++)
    {
        MyFlash_ProgramHalfWord(SavePageAddress +i *2, SaveData[i]);
    }
}

void Store_Clear(void)
{
    for (int i = 1; i < SaveSize; i++)
    {
        SaveData[i] = 0x0000;
    }
    Store_Save();
}