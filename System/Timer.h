//
// Created by Administrator on 2023/12/3/003.
//

#ifndef STM32_STANDARD_TIMER_H
#define STM32_STANDARD_TIMER_H

#include "stm32f10x.h"
void Internal_Timer_Init(void);
void ETR_Timer_Init(void);
uint16_t Timer_GetCounter(void);

#endif //STM32_STANDARD_TIMER_H
