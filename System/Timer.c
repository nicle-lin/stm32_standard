//
// Created by Administrator on 2023/12/3/003.
//

#include "Timer.h"


// 内部时钟源TIM2
void Internal_Timer_Init(void)
{
    // 开启通用定时器TIM2的时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    // 选择内部时钟源，不写也可以，因为默认选择内部时钟源
    TIM_InternalClockConfig(TIM2);

    // 配置时基单元，定时1秒钟
    TIM_TimeBaseInitTypeDef TIM_TimerBaseInitStructure;
    TIM_TimerBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1; // 外部时钟输入滤波器分频，此时不需要管，随便填,因为我们选择内部时钟源
    TIM_TimerBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up; // 向上计数
    TIM_TimerBaseInitStructure.TIM_Period = 10000-1; // 计数器
    TIM_TimerBaseInitStructure.TIM_Prescaler = 7200 -1; // 分频系数
    TIM_TimerBaseInitStructure.TIM_RepetitionCounter = 0; // 高级定时器才有的参数，随便填，不用管
    TIM_TimeBaseInit(TIM2, &TIM_TimerBaseInitStructure);

    // 清除更新中断标志位, 不清除的话，会导致一上电就进入中断，因为TIM_TimeBaseInit函数最后一行手动产生了一次中断
    TIM_ClearFlag(TIM2, TIM_FLAG_Update);

    // 使能中断
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

    // 中断分组，这个函数在main里已经初始化，所以此时直接注释掉
    //NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

    // 初始化中断
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_Init(&NVIC_InitStructure);

    // 开启定时器
    TIM_Cmd(TIM2, ENABLE);
}


// ETR时钟源TIM2
void ETR_Timer_Init(void)
{
    // 开启通用定时器TIM2的时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    // 初始化外部时钟的引脚PA0
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // 输入模式，此参数没用
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;  // PA0是TIM2 ETR输入引脚
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; // 上拉输入
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // 选择外部时钟源，模式为2
    TIM_ETRClockMode2Config(TIM2, TIM_ExtTRGPSC_OFF, TIM_ExtTRGPolarity_NonInverted, 0x00);

    // 配置时基单元，定时1秒钟
    TIM_TimeBaseInitTypeDef TIM_TimerBaseInitStructure;
    TIM_TimerBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1; // 外部时钟输入滤波器分频，此时不需要管，随便填,因为我们选择内部时钟源
    TIM_TimerBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up; // 向上计数
    TIM_TimerBaseInitStructure.TIM_Period = 10-1; // 计数器
    TIM_TimerBaseInitStructure.TIM_Prescaler = 1-1; // 分频系数, 0表示不分频
    TIM_TimerBaseInitStructure.TIM_RepetitionCounter = 0; // 高级定时器才有的参数，随便填，不用管
    TIM_TimeBaseInit(TIM2, &TIM_TimerBaseInitStructure);

    // 清除更新中断标志位, 不清除的话，会导致一上电就进入中断，因为TIM_TimeBaseInit函数最后一行手动产生了一次中断
    TIM_ClearFlag(TIM2, TIM_FLAG_Update);

    // 使能中断
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

    // 中断分组，这个函数在main里已经初始化，所以此时直接注释掉
    //NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

    // 初始化中断
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_Init(&NVIC_InitStructure);

    // 开启定时器
    TIM_Cmd(TIM2, ENABLE);
}
uint16_t  Timer_GetCounter(void)
{
    return TIM_GetCounter(TIM2);
}

// 告诉编译器引用main.c外部变量
extern uint16_t Num;

/*
void TIM2_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) == SET)
    {
        Num++;
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
    }
}
 */
