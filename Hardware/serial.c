//
// Created by Administrator on 2023/12/18/018.
//

#include "serial.h"


void Serial_Init(void)
{
    // 开启USART1时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 , ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    // 初始化USART1的引脚 PA9(TX) PA10(RX)
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // 初始化USART1
    USART_InitTypeDef USART_InitStructure;
    USART_InitStructure.USART_BaudRate = 9600;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_Init(USART1, &USART_InitStructure);

    // 开启USART接收到数据时中断
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

    // 设置中断
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_Init(&NVIC_InitStructure);

    USART_Cmd(USART1, ENABLE);
}

uint8_t rxData;
uint8_t rxFlag;
void USART1_IRQHandler(void)
{
    if (USART_GetFlagStatus(USART1, USART_IT_RXNE) == SET)
    {
        rxData = USART_ReceiveData(USART1);
        rxFlag = 1;
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
    }
}

uint8_t Serial_RxFlag(void)
{
    if (rxFlag == 1)
    {
        rxFlag = 0;
        return 1;
    }
    return 0;
}

uint8_t Serial_RxData(void)
{
    return rxData;
}

void Serial_SendData(uint8_t data)
{
    USART_SendData(USART1, data);
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
}

void Serial_SendArray(uint8_t *array, uint16_t length)
{
    uint16_t i = 0;
    for (;i < length; i++)
    {
        Serial_SendData(array[i]);
    }
}

void Serial_SendString(char *str)
{
    uint16_t i = 0;
    for (; str[i] != '\0'; i++)
    {
        Serial_SendData(str[i]);
    }
}

// 获取一个数字相应的10倍数，如12345， 10的倍数就10000， 123就是100
uint32_t Serial_Pow(uint32_t num, uint8_t length)
{
    uint8_t i = 0;
    uint32_t result = 1;
    for (; i < length; i++)
    {
        result = result * 10;
    }
    return result;
}

void Serial_SendNumber(int32_t num, uint8_t length)
{
    if (num < 0 )
    {
        Serial_SendData('-');
        num = -num;
    }
    uint8_t i = 1;
   for (; i <= length; i++)
   {
       Serial_SendData((num / Serial_Pow(num, length-i)) %10 + '0');
   }
}

// 移植printf, printf底层调用fputc, 所以重写fputc即可
// Keil软件需要点击魔法棒，然后点击use MicroLIB
int fputc(int ch, FILE *f)
{
    Serial_SendData(ch);
    return ch;
}

void Serial_Printf(char *format, ...)
{
    char String[100];
    va_list arg;
    va_start(arg, format);
    vsprintf(String, format, arg);
    va_end(arg);
    Serial_SendString(String);
}