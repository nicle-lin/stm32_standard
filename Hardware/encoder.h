//
// Created by Administrator on 2023/12/2/002.
//

#ifndef STM32_STANDARD_ENCODER_H
#define STM32_STANDARD_ENCODER_H

#include "stm32f10x.h"

void Encoder_Init(void );
int16_t Get_Encoder_Count(void);

#endif //STM32_STANDARD_ENCODER_H
