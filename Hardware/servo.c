//
// Created by Administrator on 2023/12/4/004.
//

#include "servo.h"
#include "pwm.h"


void Servo_Init(void)
{
    PWM_Servo_Init();
}

void Servo_SetAngle(float Angle)
{
    PWM_SetCompare2(Angle/180 *2000 +500);
}