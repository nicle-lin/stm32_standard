//
// Created by Administrator on 2024/1/29/029.
//

#ifndef STM32_STANDARD_MY_SPI_H
#define STM32_STANDARD_MY_SPI_H

#include "stm32f10x.h"
void MySPI_Write_CS(uint8_t bitValue);
void MySPI_Write_SCK(uint8_t bitValue);
void MySPI_Write_MOSI(uint8_t bitValue);
void MySPI_Write_MOSI(uint8_t bitValue);
uint8_t MySPI_Read_MISO();
void MySPI_Init(void);
void MySPI_Start(void);
void MySPI_Stop(void);
uint8_t MySPI_SwapByte(uint8_t ByteSend);
#endif //STM32_STANDARD_MY_SPI_H
