#include "stm32f10x.h"                  // Device header
#include "Delay.h"

void Key_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; // 上拉输入
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_11; // 两个按键的引脚
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // 在输入模式，这个参数无效
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

uint8_t Key_GetNum(void)
{
	uint8_t KeyNum = 0;
	if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1) == 0) // 按键按下了
	{
		Delay_ms(20); // 一般20ms能消除抖动
		while (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1) == 0); // 按键弹回去时才会跳出while循环
		Delay_ms(20);
		KeyNum = 1;
	}
	if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_11) == 0) // 按键按下了
	{
		Delay_ms(20); // 一般20ms能消除抖动
		while (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_11) == 0); // 按键弹回去时才会跳出while循环
		Delay_ms(20);
		KeyNum = 2;
	}
    return KeyNum;
}
