//
// Created by Administrator on 2024/2/19/019.
//

#include "w25q64.h"

void W25Q64_Init(void)
{
    MySPI_Init();
}

// 读取厂商ID与设备ID
void W25Q64_ReadID(uint8_t *Mid, uint16_t *Did)
{
    MySPI_Start();
    MySPI_SwapByte(0x9F);  //读取厂商ID与设备ID的指令
    *Mid = MySPI_SwapByte(0xFF);
    *Did = MySPI_SwapByte(0xFF); // 读取设备ID的高8位
    *Did  = *Did << 8; // 把读取的8位数据左移到高8位处
    *Did |= MySPI_SwapByte(0xFF); // 读取设备ID的位8位, 读取之后Did或上低8位数据组成一个完整的16位数据
    MySPI_Stop();
}