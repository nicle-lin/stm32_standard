//
// Created by Administrator on 2023/12/6/006.
//

#include "input_capture.h"

// 输入捕获测试

void IC_Init(void)
{

    // 初始化输入捕获PWM管脚PA6
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // 初始化时基单元
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    TIM_InternalClockConfig(TIM3); // 选择默认的内部时钟
    // 初始化时基单元
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStructure.TIM_Period = 65536-1;  // ARR 自动重装载寄存器
    TIM_TimeBaseInitStructure.TIM_Prescaler = 72-1;  // PSC 预分频寄存器
    TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);

    // 初始化输入捕获单元, 通道1的， 上升沿，不交叉， 只配置通道1可测量频率
    TIM_ICInitTypeDef TIM_ICInitStructure;
    TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
    TIM_ICInitStructure.TIM_ICFilter = 0xF; // 滤高频毛刺，数值越大滤波效果更好
    TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
    TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
    TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
    TIM_ICInit(TIM3, &TIM_ICInitStructure);

    // 通道2, 下降沿，交叉, 通道1和通道2都配置可测量占空比
    TIM_ICInitStructure.TIM_Channel = TIM_Channel_2;
    TIM_ICInitStructure.TIM_ICFilter = 0xF; // 滤高频毛刺，数值越大滤波效果更好
    TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Falling;
    TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
    TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_IndirectTI;
    TIM_ICInit(TIM3, &TIM_ICInitStructure);

    // 上面通道2的初始化也可以用下面这个函数初始化
    //TIM_PWMIConfig(TIM3, &TIM_ICInitStructure);

    // 选择输入触发源
    TIM_SelectInputTrigger(TIM3, TIM_TS_TI1FP1);
    // 配置从模式， 触发源触发时自动清零计数器
    TIM_SelectSlaveMode(TIM3, TIM_SlaveMode_Reset);

    // 启动TIM3时钟
    TIM_Cmd(TIM3, ENABLE);
}

// 获取被测量的频率，目前计数频率是72MHz / 72(Prescaler值+1) = 1MHz
uint32_t IC_GetFreq(void)
{
    return 1000000 / (TIM_GetCapture1(TIM3)+1);
}

// 获取点空比， 经过实测CCR值总是少一个，所以加回去
uint32_t  IC_GetDuty(void)
{
    return (TIM_GetCapture2(TIM3)+1) * 100 / (TIM_GetCapture1(TIM3) + 1);
}