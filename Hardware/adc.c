//
// Created by Administrator on 2023/12/12/012.
//

#include "adc.h"

void ADC1_Single_Init(void)
{
    // 开启PA0与ADC1的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

    // 设置ADC的分频器, ADC最大工作频率14MHz, 72MHz/6 = 12MHz
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);

    // 初始化引脚PA0作为ADC1通道1的输入管脚
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; // ADC专用的模拟输入模式
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // 没用的引脚
    GPIO_Init(GPIOA,&GPIO_InitStructure);

    // 配置ADC哪个通道需要采样
    ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_55Cycles5);

    // ADC主要设置
    ADC_InitTypeDef ADC_InitStructure;
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; // ADC模式，独立模式，即只使用一个ADC
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; //  是否连续转换模式(单次扫描/连续扫描)
    ADC_InitStructure.ADC_ScanConvMode = DISABLE; // 是否连续扫描模式(单通道/多通道)
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; // 转换后的数据左对齐还是右对齐
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; // ADC转换触发器，目前选择无，因为是由软件触发
    ADC_InitStructure.ADC_NbrOfChannel = 1; // 目前只有一个通道需要采样
    ADC_Init(ADC1, &ADC_InitStructure);

    // 看门狗 // 暂时不用

    // 开启ADC运行
    ADC_Cmd(ADC1, ENABLE);

    // 校准
    ADC_ResetCalibration(ADC1); // 复位校准
    while (ADC_GetResetCalibrationStatus(ADC1) == SET); // 等待复位校准完成
    ADC_StartCalibration(ADC1); // 开启校准
    while (ADC_GetCalibrationStatus(ADC1) == SET); // 等待开启校准完成

}

/*
 * 转换周期: ADC_SampleTime_55Cycles5(采样周期) + 12.5周期(固定转换周期) = 68个周期
 * ADC1工作频率: 72MHz / RCC_PCLK2_Div6 = 72MHz / 6 = 12MHz
 * 转换时间: (1/12MHz) * 68 = 5.6us
 */
uint16_t ADC1_Single_GetValue(void)
{
     // 软件触发开始ADC1转换
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);
    // 判断是否转换完成
    while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
    return ADC_GetConversionValue(ADC1);
}



// 测试多通道连续转换， 但不使用DMA转运数据
void ADC1_Multi_Init(void)
{
    // 开启PA0与ADC1的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

    // 设置ADC的分频器, ADC最大工作频率14MHz, 72MHz/6 = 12MHz
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);

    // 初始化引脚PA0作为ADC1通道1的输入管脚
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; // ADC专用的模拟输入模式
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // 没用的引脚
    GPIO_Init(GPIOA,&GPIO_InitStructure);

    // 配置ADC哪个通道需要采样
    // ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_55Cycles5);

    // ADC主要设置
    ADC_InitTypeDef ADC_InitStructure;
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; // ADC模式，独立模式，即只使用一个ADC
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE; //  是否连续转换模式(单次扫描/连续扫描)
    ADC_InitStructure.ADC_ScanConvMode = DISABLE; // 是否连续扫描模式(单通道/多通道)
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; // 转换后的数据左对齐还是右对齐
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; // ADC转换触发器，目前选择无，因为是由软件触发
    ADC_InitStructure.ADC_NbrOfChannel = 4; // 目前4个通道需要采样
    ADC_Init(ADC1, &ADC_InitStructure);

    // 看门狗 // 暂时不用

    // 开启ADC运行
    ADC_Cmd(ADC1, ENABLE);

    // 校准
    ADC_ResetCalibration(ADC1); // 复位校准
    while (ADC_GetResetCalibrationStatus(ADC1) == SET); // 等待复位校准完成
    ADC_StartCalibration(ADC1); // 开启校准
    while (ADC_GetCalibrationStatus(ADC1) == SET); // 等待开启校准完成

}

/*
 * 转换周期: ADC_SampleTime_55Cycles5(采样周期) + 12.5周期(固定转换周期) = 68个周期
 * ADC1工作频率: 72MHz / RCC_PCLK2_Div6 = 72MHz / 6 = 12MHz
 * 转换时间: (1/12MHz) * 68 = 5.6us
 */
uint16_t ADC1_Multi_GetValue(uint8_t ADC_Channel)
{
    ADC_RegularChannelConfig(ADC1, ADC_Channel, 1, ADC_SampleTime_55Cycles5);

    // 软件触发开始ADC1转换
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);
    // 判断是否转换完成
    while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
    return ADC_GetConversionValue(ADC1);
}



// 测试多通道连续转换， 使用DMA转运数据
void ADC1_DMA_Init(uint32_t dstAddr, uint32_t size)
{
    // 开启PA0与ADC1的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

    // 设置ADC的分频器, ADC最大工作频率14MHz, 72MHz/6 = 12MHz
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);

    // 初始化引脚PA0作为ADC1通道1的输入管脚
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; // ADC专用的模拟输入模式
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // 没用的引脚
    GPIO_Init(GPIOA,&GPIO_InitStructure);

    // 配置ADC哪个通道需要采样
    ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_55Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 2, ADC_SampleTime_55Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 3, ADC_SampleTime_55Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_3, 4, ADC_SampleTime_55Cycles5);

    // ADC主要设置
    ADC_InitTypeDef ADC_InitStructure;
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; // ADC模式，独立模式，即只使用一个ADC
    // ADC_InitStructure.ADC_ContinuousConvMode = ENABLE; //  是否连续转换模式(单次扫描/连续扫描)
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; //  是否连续转换模式(单次扫描/连续扫描)
    ADC_InitStructure.ADC_ScanConvMode = ENABLE; // 是否连续扫描模式(单通道/多通道)
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; // 转换后的数据左对齐还是右对齐
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; // ADC转换触发器，目前选择无，因为是由软件触发
    ADC_InitStructure.ADC_NbrOfChannel = 4; // 目前4个通道需要采样
    ADC_Init(ADC1, &ADC_InitStructure);


    /* 配置DMA, 需要满足三个条件才能工作，
     * 1. DMA_Cmd要ENABLE,
     * 2. DMA_InitStructure.DMA_BufferSize计数器要大于0
     * 3. 触发源触发，这里即ADC转换完成
     */
    // 初始化DMA
    DMA_InitTypeDef DMA_InitStructure;
    DMA_InitStructure.DMA_BufferSize = size; // 计数器值
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; // 传输方向, 从ADC寄存器里转运
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  // 这里要disable， 因为是从寄存器转移到存储器
    DMA_InitStructure.DMA_MemoryBaseAddr = dstAddr;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord; // 位宽是半字
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; // 传输完一个之后自增地址，
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR; // ADC1转换完成之后存储的寄存器
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal; // 计数器值减到0之后不自动重装数值
    //DMA_InitStructure.DMA_Mode = DMA_Mode_Circular; // 计数器值减到0之后不自动重装数值
    DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);

    // 开启DMA工作
    DMA_Cmd(DMA1_Channel1, ENABLE);

    // 开启ADC到DMA的触发器
    ADC_DMACmd(ADC1, ENABLE);


    // 看门狗 // 暂时不用

    // 开启ADC运行
    ADC_Cmd(ADC1, ENABLE);

    // 校准
    ADC_ResetCalibration(ADC1); // 复位校准
    while (ADC_GetResetCalibrationStatus(ADC1) == SET); // 等待复位校准完成
    ADC_StartCalibration(ADC1); // 开启校准
    while (ADC_GetCalibrationStatus(ADC1) == SET); // 等待开启校准完成

    // ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}



void ADC1_DMA_GetValue(uint16_t size)
{
    DMA_Cmd(DMA1_Channel1, DISABLE);
    DMA_SetCurrDataCounter(DMA1_Channel1, size);
    DMA_Cmd(DMA1_Channel1, ENABLE);

    // 软件触发开始ADC1转换
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);

    // 等待DMA转运完成
    // 判断是否转换完成, 如果发生错误退出while,或者转运完成退出while
    while (DMA_GetFlagStatus(DMA1_FLAG_TC1) == RESET || DMA_GetFlagStatus(DMA1_FLAG_TE1)  == SET);
    if (DMA_GetFlagStatus(DMA1_FLAG_TE1) == SET)
    {
        // 发生了错误，怎么处理
    }
    else
    {
        DMA_ClearFlag(DMA1_FLAG_TC1); // 需要手动清除标志位
        DMA_ClearFlag(DMA1_FLAG_TE1); // 需要手动清除标志位
    }
}

