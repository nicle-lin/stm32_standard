//
// Created by Administrator on 2023/11/30/030.
//

#include "light_buzzer.h"

// 第一个程序，依次点亮走马灯和使蜂鸣器响
int light_and_buzzer(void)
{

    /* 使用直接配置寄存器点亮灯
    RCC -> APB0ENR = 0x00000010;
    GPIOC -> CRH = 0x002ffffe;
    GPIOC -> ODR = 0xfffffffffffffffe;
    */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC, ENABLE);

    // 初始化管脚PA-2~PA6
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;  // 推挽输出
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 ;  // 灯的管脚
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA,&GPIO_InitStruct);


    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;  // 推挽输出
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_11;  // 灯的管脚
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC,&GPIO_InitStruct);
    GPIO_ResetBits(GPIOC, GPIO_Pin_11);

    // 管脚PB10接蜂鸣器
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;  // 推挽输出
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB,&GPIO_InitStruct);
    GPIO_ResetBits(GPIOB, GPIO_Pin_10);

    while(1)
    {
        // 以下代码是依次点亮管脚PA-2~PA6
        GPIO_Write(GPIOA, ~GPIO_Pin_2);
        Delay_ms(500);
        GPIO_Write(GPIOA, ~GPIO_Pin_1);
        Delay_ms(500);
        GPIO_Write(GPIOA, ~GPIO_Pin_0);
        Delay_ms(500);
        GPIO_Write(GPIOA, ~GPIO_Pin_1);
        Delay_ms(500);
        GPIO_Write(GPIOA, ~GPIO_Pin_2);
        Delay_ms(500);
        GPIO_Write(GPIOA, ~GPIO_Pin_3);
        Delay_ms(500);
        GPIO_Write(GPIOA, ~GPIO_Pin_4);
        Delay_ms(500);
    }
}
