//
// Created by Administrator on 2023/12/9/009.
//

#ifndef STM32_STANDARD_TIM_ENCODER_H
#define STM32_STANDARD_TIM_ENCODER_H

#include "stm32f10x.h"

void Tim_Encoder_Init(void);
int16_t Encoder_GetCounter(void);
int16_t Encoder_GetSpeed(void);

#endif //STM32_STANDARD_TIM_ENCODER_H
