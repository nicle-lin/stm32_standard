//
// Created by Administrator on 2023/12/12/012.
//

#ifndef STM32_STANDARD_ADC_H
#define STM32_STANDARD_ADC_H

#include "stm32f10x.h"

void ADC1_Single_Init(void);
uint16_t ADC1_Single_GetValue(void);

void ADC1_Multi_Init(void);
uint16_t ADC1_Multi_GetValue(uint8_t ADC_Channel);

void ADC1_DMA_Init(uint32_t dstAddr, uint32_t size);
void ADC1_DMA_GetValue(uint16_t size);

#endif //STM32_STANDARD_ADC_H
