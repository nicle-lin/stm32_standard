//
// Created by Administrator on 2023/12/6/006.
//

#ifndef STM32_STANDARD_INPUT_CAPTURE_H
#define STM32_STANDARD_INPUT_CAPTURE_H

#include "stm32f10x.h"

void IC_Init(void);
uint32_t IC_GetFreq(void);
uint32_t  IC_GetDuty(void);

#endif //STM32_STANDARD_INPUT_CAPTURE_H
