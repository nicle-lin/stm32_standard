//
// Created by Administrator on 2023/12/16/016.
//

#include "dma.h"


// 内存之间传输
void DMA_M2M_Init(uint32_t srcAddr, uint32_t dstAddr, uint32_t size)
{
    // 开启DMA时钟
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

    // 初始化DMA
    DMA_InitTypeDef DMA_InitStructure;
    DMA_InitStructure.DMA_BufferSize = size; // 计数器值
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; // 传输方向
    DMA_InitStructure.DMA_M2M = DMA_M2M_Enable;  // 存储器--->存储器之间传输
    DMA_InitStructure.DMA_MemoryBaseAddr = dstAddr;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte; // 位宽是字节
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; // 传输完一个之后自增地址，
    DMA_InitStructure.DMA_PeripheralBaseAddr = srcAddr;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Enable;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal; // 计数器值减到0之后不自动重装数值
    DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;

    DMA_Init(DMA1_Channel1, &DMA_InitStructure);

    DMA_Cmd(DMA1_Channel1, DISABLE);
}

void DMA_M2M_Transfer(uint16_t size)
{
    DMA_Cmd(DMA1_Channel1, DISABLE); // 需要停止DMA才能执行下一句
    DMA_SetCurrDataCounter(DMA1_Channel1, size);
    DMA_Cmd(DMA1_Channel1, ENABLE);

    // 判断是否转换完成, 如果发生错误退出while,或者转运完成退出while
    while (DMA_GetFlagStatus(DMA1_FLAG_TC1) == RESET || DMA_GetFlagStatus(DMA1_FLAG_TE1)  == SET);
    if (DMA_GetFlagStatus(DMA1_FLAG_TE1) == SET)
    {
        // 发生了错误，怎么处理
    }
    else
    {
        DMA_ClearFlag(DMA1_FLAG_TC1); // 需要手动清除标志位
        DMA_ClearFlag(DMA1_FLAG_TE1); // 需要手动清除标志位
    }
}
