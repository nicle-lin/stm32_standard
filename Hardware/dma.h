//
// Created by Administrator on 2023/12/16/016.
//

#ifndef STM32_STANDARD_DMA_H
#define STM32_STANDARD_DMA_H

#include "stm32f10x.h"
void DMA_M2M_Init(uint32_t srcAddr, uint32_t dstAddr, uint32_t size);
void DMA_M2M_Transfer(uint16_t size);


#endif //STM32_STANDARD_DMA_H
