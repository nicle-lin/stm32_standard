//
// Created by Administrator on 2023/12/4/004.
//

#ifndef STM32_STANDARD_MOTOR_H
#define STM32_STANDARD_MOTOR_H

#include "stm32f10x.h"
#include "pwm.h"
void Motor_Init(void);
void Motor_SetSpeed(int8_t speed);

#endif //STM32_STANDARD_MOTOR_H
