//
// Created by Administrator on 2023/12/2/002.
//

#ifndef STM32_STANDARD_COUNTSENSOR_H
#define STM32_STANDARD_COUNTSENSOR_H

#include "stm32f10x.h"
void CountSensor_Init(void );
uint16_t Get_CountSensor(void);

#endif //STM32_STANDARD_COUNTSENSOR_H
