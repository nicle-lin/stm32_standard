//
// Created by Administrator on 2024/1/14/014.
//

#ifndef STM32_STANDARD_MY_I2C_H
#define STM32_STANDARD_MY_I2C_H

#include "stm32f10x.h"



void MyI2C_Init(void);
void MyI2C_Start(void);
void MyI2C_Stop(void);
void MyI2C_SendByte(uint8_t Byte);
uint8_t MyI2C_ReceiveByte(void);
void MyI2C_SendAck(uint8_t AckBit);
uint8_t MyI2C_ReceiveAck(void);

#endif //STM32_STANDARD_MY_I2C_H
