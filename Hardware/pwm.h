//
// Created by Administrator on 2023/12/3/003.
//

#ifndef STM32_STANDARD_PWM_H
#define STM32_STANDARD_PWM_H

#include "stm32f10x.h"
void PWM_Init(void);
void PWM_SetCompare1(uint16_t compare1);
void PWM_Servo_Init(void);
void PWM_SetCompare2(uint16_t compare2);
void PWM_Motor_Init(void);
void PWM_SetCompare3(uint16_t compare3);

void PWM_Input_Init(void);
void PWM_Input_SetCompare(uint16_t compare);
void PWM_Input_SetPrescaler(uint16_t prescaler);
#endif //STM32_STANDARD_PWM_H
