//
// Created by Administrator on 2023/12/4/004.
//

#ifndef STM32_STANDARD_SERVO_H
#define STM32_STANDARD_SERVO_H

#include "stm32f10x.h"

void Servo_Init(void);
void Servo_SetAngle(float Angle);

#endif //STM32_STANDARD_SERVO_H
