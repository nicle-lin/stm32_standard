//
// Created by Administrator on 2024/2/19/019.
//

#ifndef STM32_STANDARD_W25Q64_H
#define STM32_STANDARD_W25Q64_H

#include "stm32f10x.h"
#include "my_spi.h"

void W25Q64_Init(void);
void W25Q64_ReadID(uint8_t *Mid, uint16_t *Did);

#endif //STM32_STANDARD_W25Q64_H
