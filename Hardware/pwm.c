//
// Created by Administrator on 2023/12/3/003.
//

#include "pwm.h"

// 输出PWM波形驱动呼吸灯
void PWM_Init(void)
{
    // 使用重映射的功能，把TIM2 CH1输出的管脚PA0重映射到PA15
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    // 把TIM2_CH1_ETR(PA0)重映射到PA15
    GPIO_PinRemapConfig(GPIO_PartialRemap1_TIM2, ENABLE);
    // 解除JTAG功能，释放管脚PA15/PB3/PB4，改为普通GPIO使用，因为这些管脚默认功能是JTAG
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

    // 如果不使用重映射功能那只需要初始化管脚PA0，不用上面三行代码，注释掉即可
    // 把下面GPIO_Pin = GPIO_Pin_15改为GPIO_Pin = GPIO_Pin_0

    // 初始化输出PWM波形的管脚PA15
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; // 复用推挽输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

     // 初始化时基单元
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_InternalClockConfig(TIM2); // 选择默认的内部时钟

    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStructure.TIM_Period = 100-1;  // ARR 自动重装载寄存器
    TIM_TimeBaseInitStructure.TIM_Prescaler = 720 -1;  // PSC 预分频寄存器
    TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);

    // 设置输出比较
    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_OCStructInit(&TIM_OCInitStructure); // 先给结构体赋默认值
    // 选取以下几都是通用定时器的参数，其它没有选择的是高级定时器的
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; // 高电平有效
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0; // CCR
    TIM_OC1Init(TIM2, &TIM_OCInitStructure);

    TIM_Cmd(TIM2, ENABLE); // 时钟开始时钟
}


// 设置CCR的值
void PWM_SetCompare1(uint16_t compare1)
{
    TIM_SetCompare1(TIM2, compare1);
}


// 输出PWM波形驱动舵机， 舵机要求输出频率为50Hz(即20ms), 高电平宽度为0.5ms~2.5ms
void PWM_Servo_Init(void)
{

    // 初始化输出PWM波形的管脚PA1
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; // 复用推挽输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // 初始化时基单元
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_InternalClockConfig(TIM2); // 选择默认的内部时钟

    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStructure.TIM_Period = 20000-1;  // ARR 自动重装载寄存器
    TIM_TimeBaseInitStructure.TIM_Prescaler = 72-1;  // PSC 预分频寄存器
    TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);

    // 设置输出比较
    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_OCStructInit(&TIM_OCInitStructure); // 先给结构体赋默认值
    // 选取以下几都是通用定时器的参数，其它没有选择的是高级定时器的
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; // 高电平有效
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0; // CCR
    TIM_OC2Init(TIM2, &TIM_OCInitStructure); // 使用通道2

    TIM_Cmd(TIM2, ENABLE); // 时钟开始时钟
}

// 设置CCR的值
void PWM_SetCompare2(uint16_t compare2)
{
    TIM_SetCompare2(TIM2, compare2);
}

// 输出PWM波形驱动直流电机
void PWM_Motor_Init(void)
{

    // 初始化输出PWM波形的管脚PA2
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; // 复用推挽输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // 初始化时基单元
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_InternalClockConfig(TIM2); // 选择默认的内部时钟

    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStructure.TIM_Period = 100-1;  // ARR 自动重装载寄存器
    TIM_TimeBaseInitStructure.TIM_Prescaler = 720-1;  // PSC 预分频寄存器
    TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);

    // 设置输出比较
    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_OCStructInit(&TIM_OCInitStructure); // 先给结构体赋默认值
    // 选取以下几都是通用定时器的参数，其它没有选择的是高级定时器的
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; // 高电平有效
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0; // CCR
    TIM_OC3Init(TIM2, &TIM_OCInitStructure); // 使用通道3

    TIM_Cmd(TIM2, ENABLE); // 时钟开始时钟
}

// 设置CCR的值
void PWM_SetCompare3(uint16_t compare3)
{
    TIM_SetCompare3(TIM2, compare3);
}


// 输出PWM波形用于输入捕获，输出引脚是PA0, 输入引脚是PA6
void PWM_Input_Init(void)
{

    // 初始化输出PWM波形的管脚PA0
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; // 复用推挽输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // 初始化时基单元
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_InternalClockConfig(TIM2); // 选择默认的内部时钟

    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStructure.TIM_Period = 100-1;  // ARR 自动重装载寄存器
    TIM_TimeBaseInitStructure.TIM_Prescaler = 720-1;  // PSC 预分频寄存器
    TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);

    // 设置输出比较
    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_OCStructInit(&TIM_OCInitStructure); // 先给结构体赋默认值
    // 选取以下几都是通用定时器的参数，其它没有选择的是高级定时器的
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; // 高电平有效
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0; // CCR
    TIM_OC1Init(TIM2, &TIM_OCInitStructure); // 使用通道1

    TIM_Cmd(TIM2, ENABLE); // 时钟开始时钟
}

// 设置CCR的值
void PWM_Input_SetCompare(uint16_t compare)
{
    TIM_SetCompare1(TIM2, compare);
}

void PWM_Input_SetPrescaler(uint16_t prescaler)
{
    TIM_PrescalerConfig(TIM2, prescaler,TIM_PSCReloadMode_Immediate);
}


