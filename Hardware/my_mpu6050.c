//
// Created by Administrator on 2024/1/14/014.
//

#include "my_mpu6050.h"

#define MPU6050_ADDRESS 0xD0

void MPU6050_WriteReg(uint8_t regAddress, uint8_t Data)
{
    MyI2C_Start();
    MyI2C_SendByte(MPU6050_ADDRESS);
    MyI2C_ReceiveAck(); // 需要判断是否有回复，如果没有回复说明此地址不存在
    MyI2C_SendByte(regAddress);
    MyI2C_ReceiveAck();
    MyI2C_SendByte(Data);
    MyI2C_ReceiveAck();
    MyI2C_Stop();
}

uint8_t MPU6050_ReadReg(uint8_t regAddress)
{
    MyI2C_Start();
    MyI2C_SendByte(MPU6050_ADDRESS);
    MyI2C_ReceiveAck(); // 需要判断是否有回复，如果没有回复说明此地址不存在
    MyI2C_SendByte(regAddress);
    MyI2C_ReceiveAck();
    // 重复启动
    MyI2C_Start();
    MyI2C_SendByte(MPU6050_ADDRESS | 0x01);  // 改为写权限
    MyI2C_ReceiveAck();
    uint8_t Data = MyI2C_ReceiveByte();
    MyI2C_SendAck(1); // 只读一个数据，所以发送1为不应答
    MyI2C_Stop();
    return Data;
}

void MPU6050_Init(void)
{
    MyI2C_Init();
    MPU6050_WriteReg(MPU6050_PWR_MGMT_1, 0x01);
    MPU6050_WriteReg(MPU6050_PWR_MGMT_2, 0x00);
    MPU6050_WriteReg(MPU6050_SMPLRT_DIV, 0x09);
    MPU6050_WriteReg(MPU6050_CONFIG, 0x06);
    MPU6050_WriteReg(MPU6050_GYRO_CONFIG, 0x18);
    MPU6050_WriteReg(MPU6050_ACCEL_CONFIG, 0x18);

}

// 使用硬件I2C读取MPU6050的初始化
void MPU6050_Hardware_Init(void)
{
    // 开启I2C和GPIOB的时钟， I2C2对应的管脚是PB10，PB11
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD; // 复用开漏输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    I2C_InitTypeDef I2C_InitStructure;
    I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
    I2C_InitStructure.I2C_ClockSpeed = 50000; // 50KHz, 支持最大的速度为400KHz
    // 由于上升沿不能立即上升，所以速度越快，需要更长的时间给数据线翻转电平。所以需要低电平占比更高
    I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2; // 低电平:高电平 = 2:1,
    I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
    I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2C_InitStructure.I2C_OwnAddress1 = 0x00;// 做为从机时才需要使用，所以随便给个值，不冲突即可
    I2C_Init(I2C2, &I2C_InitStructure);

    I2C_Cmd(I2C2, ENABLE);
}

// 超时等待退出函数
void MPU6050_WaitEvent(I2C_TypeDef* I2Cx, uint32_t I2C_EVENT)
{
    uint32_t timout = 10000;
    while (I2C_CheckEvent(I2Cx, I2C_EVENT) != SUCCESS)
    {
        timout --;
        if (timout == 0)
        {
            break;
        }
    }

}

void MPU6050_Hardware_WriteReg(uint8_t regAddress, uint8_t Data)
{
    I2C_GenerateSTART(I2C2, ENABLE);
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT); // EV5事件

    I2C_Send7bitAddress(I2C2, MPU6050_ADDRESS, I2C_Direction_Transmitter); // 发送地址
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED); // EV6事件，发送模式

    I2C_SendData(I2C2, regAddress); // 发送写入MPU6050的寄存器地址
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTING) ; // EV8事件, 正在发送数据

    I2C_SendData(I2C2, Data);
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED); // EV8事件, 发送完成

    I2C_GenerateSTOP(I2C2, ENABLE); // 产生停止信号

}



uint8_t MPU6050_Hardware_ReadReg(uint8_t regAddress)
{
    I2C_GenerateSTART(I2C2, ENABLE);
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT); // EV5事件

    I2C_Send7bitAddress(I2C2, MPU6050_ADDRESS, I2C_Direction_Transmitter); // 发送地址
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED); // EV6事件，发送模式

    I2C_SendData(I2C2, regAddress); // 发送写入MPU6050的寄存器地址
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTING); // EV8事件, 正在发送数据

    // 重新产生起始信号
    I2C_GenerateSTART(I2C2, ENABLE);
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT); // EV5事件

    I2C_Send7bitAddress(I2C2, MPU6050_ADDRESS, I2C_Direction_Receiver); // 接收
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED); // EV6事件，接收模式

    // 接收一个字节，此时就需要提示置标志位不应答, 产生停止信号
    I2C_AcknowledgeConfig(I2C2, DISABLE);
    I2C_GenerateSTOP(I2C2, ENABLE); // 产生停止信号
    MPU6050_WaitEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED); // EV7事件, 接收数据是否完成

    uint8_t data = I2C_ReceiveData(I2C2);

    I2C_AcknowledgeConfig(I2C2, ENABLE); // 恢复默认应答，为了下一次接收数据

    return data;
}

void MPU6050_GetData(int16_t *accX, int16_t *accY, int16_t *accZ, int16_t *gyroX, int16_t *gyroY, int16_t *gyroZ)
{
    uint8_t dataH, dataL;

    dataH = MPU6050_Hardware_ReadReg(MPU6050_ACCEL_XOUT_H);
    dataL = MPU6050_Hardware_ReadReg(MPU6050_ACCEL_XOUT_L);
    *accX = (dataH << 8) | dataL; // 正常dataH为8位变量左移8位之后会溢出，但由于赋值给16位的accX，所以不会有问题

    dataH = MPU6050_Hardware_ReadReg(MPU6050_ACCEL_YOUT_H);
    dataL = MPU6050_Hardware_ReadReg(MPU6050_ACCEL_YOUT_L);
    *accY = (dataH << 8) | dataL;

    dataH = MPU6050_Hardware_ReadReg(MPU6050_ACCEL_ZOUT_H);
    dataL = MPU6050_Hardware_ReadReg(MPU6050_ACCEL_ZOUT_L);
    *accZ = (dataH << 8) | dataL;

    dataH = MPU6050_Hardware_ReadReg(MPU6050_GYRO_XOUT_H);
    dataL = MPU6050_Hardware_ReadReg(MPU6050_GYRO_XOUT_L);
    *gyroX= (dataH << 8) | dataL;

    dataH = MPU6050_Hardware_ReadReg(MPU6050_GYRO_YOUT_H);
    dataL = MPU6050_Hardware_ReadReg(MPU6050_GYRO_YOUT_L);
    *gyroY= (dataH << 8) | dataL;

    dataH = MPU6050_Hardware_ReadReg(MPU6050_GYRO_ZOUT_H);
    dataL = MPU6050_Hardware_ReadReg(MPU6050_GYRO_ZOUT_L);
    *gyroZ= (dataH << 8) | dataL;

}