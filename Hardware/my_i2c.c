//
// Created by Administrator on 2024/1/14/014.
//

#include "my_i2c.h"
#include "Delay.h"

void MyI2C_Write_SCL(uint8_t BitValue)
{
    GPIO_WriteBit(GPIOA, GPIO_Pin_6, (BitAction)BitValue);
    Delay_us(10); // 防止MPU6050跟不上速度
}

void MyI2C_Write_SDA(uint8_t BitValue)
{
    GPIO_WriteBit(GPIOA, GPIO_Pin_7, (BitAction)BitValue);
    Delay_us(10); // 防止MPU6050跟不上速度
}

uint8_t MyI2C_Read_SDA(void)
{
    uint8_t bitValue = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7);
    Delay_us(10); // 防止MPU6050跟不上速度
    return bitValue;
}



void MyI2C_Init(void)
{
    // PA7为SDA, PA6为SCL
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD; // 开漏输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_SetBits(GPIOA, GPIO_Pin_6 | GPIO_Pin_7);
}

void MyI2C_Start(void)
{
    // 先停止I2C
    MyI2C_Write_SDA(1);
    MyI2C_Write_SCL(1);

    // 开启I2C
    MyI2C_Write_SDA(0);
    MyI2C_Write_SCL(0);
}

void MyI2C_Stop(void)
{
    MyI2C_Write_SDA(0);  // 先拉低SDA, 然后再拉高SDA才能产生一个上升沿
    MyI2C_Write_SCL(1);
    MyI2C_Write_SDA(1);
}

void MyI2C_SendByte(uint8_t Byte)
{
    for (uint8_t i = 0; i < 8; i++)
    {
        MyI2C_Write_SDA(Byte & (0x80 >> i));
        MyI2C_Write_SCL(1);
        MyI2C_Write_SCL(0);
    }
}

uint8_t MyI2C_ReceiveByte(void)
{
    uint8_t i, Byte = 0x00;
    MyI2C_Write_SDA(1); // 主机释放SDA，改为输入模式

    // 开始读
    for (i = 0; i < 8; i++)
    {
        MyI2C_Write_SCL(1);
        if (MyI2C_Read_SDA() == 1)
        {
            Byte |= 0x80 >> i;
        }
        MyI2C_Write_SCL(0);
    }
    return Byte;

}


void MyI2C_SendAck(uint8_t AckBit)
{
    MyI2C_Write_SDA(AckBit);
    MyI2C_Write_SCL(1);
    MyI2C_Write_SCL(0);
}

uint8_t MyI2C_ReceiveAck(void)
{
    MyI2C_Write_SDA(1); // 主机释放SDA，改为输入模式

    // 开始读
    MyI2C_Write_SCL(1);
    uint8_t AckBit =  MyI2C_Read_SDA();
    MyI2C_Write_SCL(0);
    return AckBit;

}