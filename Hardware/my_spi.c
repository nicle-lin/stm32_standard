//
// Created by Administrator on 2024/1/29/029.
//

#include "my_spi.h"



// 以下操作由于SPI速度非常快，所以不需要加延时

void MySPI_Write_CS(uint8_t bitValue)
{
    GPIO_WriteBit(GPIOA, GPIO_Pin_4, (BitAction)bitValue);
}

void MySPI_Write_SCK(uint8_t bitValue)
{
    GPIO_WriteBit(GPIOA, GPIO_Pin_5, (BitAction)bitValue);
}


void MySPI_Write_MOSI(uint8_t bitValue)
{
    GPIO_WriteBit(GPIOA, GPIO_Pin_7, (BitAction)bitValue);
}


uint8_t MySPI_Read_MISO()
{
    return GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6);
}


void MySPI_Init(void)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; // 上拉输入
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6; // MISO引脚，输入引脚
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // 在输入模式，这个参数无效
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; // 推挽输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_7; // MOSI引脚，CS片选引脚，CLK引脚
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    MySPI_Write_CS(1);  // 片选引脚默认为高电平，即没有选中
    MySPI_Write_SCK(0); // 选择模式0，所以需要置0
}

void MySPI_Start(void)
{
    MySPI_Write_CS(0);
}


void MySPI_Stop(void)
{
    MySPI_Write_CS(1);
}

// 交换一个字节
uint8_t MySPI_SwapByte(uint8_t ByteSend)
{
    uint8_t i, ByteReceive = 0x00;

    for (i = 0; i < 8; i++)
    {
        // 写入一位，高位先行
        MySPI_Write_MOSI(ByteSend & (0x0 >> i));
        MySPI_Write_SCK(1);
        if (MySPI_Read_MISO() == 1)
        {
            ByteReceive |= (0x80 >> i);  // 先接收到高位
        }
        MySPI_Write_SCK(0);

    }
    return ByteReceive;
}