//
// Created by Administrator on 2023/12/18/018.
//

#ifndef STM32_STANDARD_SERIAL_H
#define STM32_STANDARD_SERIAL_H

#include "stm32f10x.h"
#include <stdio.h>
#include <stdarg.h>
void Serial_Init(void);
void Serial_SendData(uint8_t data);
void Serial_SendArray(uint8_t *array, uint16_t length);
void Serial_SendString(char *str);
void Serial_SendNumber(int32_t num, uint8_t length);

uint8_t Serial_RxFlag(void);
uint8_t Serial_RxData(void);

#endif //STM32_STANDARD_SERIAL_H
