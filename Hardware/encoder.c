//
// Created by Administrator on 2023/12/2/002.
//

#include "encoder.h"

// 外部中断EXTI测试
void Encoder_Init(void )
{
    // 打开外设GPIOB及AFIO的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);

    // 初始化GPIO
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; // 可以上拉/下拉/浮空输入模式，此处选择上拉输入
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // 输入模式可不关心此参数
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    // 配置AFIO， 选择哪个引脚做为EXTI输入
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource0);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource1);

    // 配置EXTI
    EXTI_InitTypeDef  EXTI_InitStructure;
    EXTI_InitStructure.EXTI_Line = EXTI_Line0 | EXTI_Line1;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling ;
    EXTI_Init(&EXTI_InitStructure);

    // 配置NVIC
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_Init(&NVIC_InitStructure);

}

int16_t Encoder_Count;

int16_t Get_Encoder_Count(void)
{
    uint16_t tmp = Encoder_Count;
    Encoder_Count = 0;
    return tmp;
}

void EXTI0_IRQHandler(void)
{
   if (EXTI_GetITStatus(EXTI_Line0) == SET)
   {
       if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1) == 0)
       {
           Encoder_Count++;
       }
	   EXTI_ClearITPendingBit(EXTI_Line0);
   }
}


void EXTI1_IRQHandler(void)
{
    if (EXTI_GetITStatus(EXTI_Line1) == SET)
    {
        if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_0) == 0)
        {
            Encoder_Count--;
        }
		EXTI_ClearITPendingBit(EXTI_Line1);
    }
}