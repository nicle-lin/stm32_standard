//
// Created by Administrator on 2023/12/9/009.
//
#include "tim_encoder.h"

// 定时器编码器

void Tim_Encoder_Init(void)
{

    // 初始化TIM3定时器chanel1与channel2的管脚PA6和PA7
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // 初始化时基单元
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    // TIM_InternalClockConfig(TIM3); // 不需要选择时钟，因为编码器会托管时钟，编码器就是一个带方向控制的外部时钟
    // 初始化时基单元
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;  // 这个参数没用，被编码器托管了
    TIM_TimeBaseInitStructure.TIM_Period = 65536-1;  // ARR 自动重装载寄存器，设置为65535计数的范围是最大，而且也方便换算为负数(补码)
    TIM_TimeBaseInitStructure.TIM_Prescaler = 1-1;  // PSC 预分频寄存器,不分频
    TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);

    // 初始化输入捕获单元
    TIM_ICInitTypeDef TIM_ICInitStructure;
    TIM_ICStructInit(&TIM_ICInitStructure); // 给参数默认值
    TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
    TIM_ICInitStructure.TIM_ICFilter = 0xF; // 滤高频毛刺，数值越大滤波效果更好
    //TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising; // 代表高低电平不反转, 此参数可在函数TIM_EncoderInterfaceConfig配置
    //TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1; // 这个参数没用
    //TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI; // 这个参数没用
    TIM_ICInit(TIM3, &TIM_ICInitStructure);

    TIM_ICInitStructure.TIM_Channel = TIM_Channel_2;
    TIM_ICInitStructure.TIM_ICFilter = 0xF; // 滤高频毛刺，数值越大滤波效果更好
    //TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising; // 代表高低电平不反转, 此参数可在函数TIM_EncoderInterfaceConfig配置
    //TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
    //TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_IndirectTI;
    TIM_ICInit(TIM3, &TIM_ICInitStructure);


    // 配置编码器接口, 此函数需要在TIM_ICInit后面执行, 因为这两个函数都有操作同一个寄存器
    TIM_EncoderInterfaceConfig(TIM3, TIM_EncoderMode_TI12,TIM_ICPolarity_Rising,TIM_ICPolarity_Rising);

    // 启动TIM3时钟
    TIM_Cmd(TIM3, ENABLE);
}

// 利用补码的特性，16位的65535对应就是-1, TIM_GetCounter返回是无符号uint16_t，强制转换为int16_t即可显示负数
int16_t Encoder_GetCounter(void)
{
    return TIM_GetCounter(TIM3);
}

// 测试速度，单位时间内计数
int16_t Encoder_GetSpeed(void)
{
    // 获取当前计数之后需要清零
    int16_t tmp = TIM_GetCounter(TIM3);
    TIM_SetCounter(TIM3, 0);
    return tmp;

}